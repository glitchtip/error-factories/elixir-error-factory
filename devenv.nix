{
  pkgs,
  lib,
  ...
}: let
  erlang = pkgs.beam.packages.erlangR26;
  elixir = erlang.elixir_1_16;
  node = pkgs.nodejs_20;
  elixir-ls = (erlang.elixir-ls.override {elixir = elixir;}).overrideAttrs (prev: {
    version = "master";
    src = pkgs.fetchFromGitHub {
      owner = "elixir-lsp";
      repo = "elixir-ls";
      rev = "06178f5cc5babad9522ba1dc67046ffc48a0d6ed";
      hash = "sha256-8tFQ68gxlHjzCfgdB5QFaxMoelN5Ht4XmqtuM8Fwg2E=";
      fetchSubmodules = true;
    };
    buildPhase = ''
      runHook preBuild
      mix do compile --no-deps-check, elixir_ls.release2
      runHook postBuild
    '';
  });
in {
  dotenv.enable = true;

  env.LANG = "en_US.UTF-8";
  env.ERL_AFLAGS = "-kernel shell_history enabled";

  enterShell = ''
    export MIX_HOME=$PWD/.nix-mix
    export HEX_HOME=$PWD/.nix-hex
    export PATH=$MIX_HOME/bin:$PATH
    export PATH=$HEX_HOME/bin:$PATH
    export PATH=$PATH:$(pwd)/_build/pip_packages/bin
  '';

  packages = with pkgs;
    [
      elixir-ls
      # next-ls.packages.x86_64-linux.default
    ]
    ++ lib.optionals pkgs.stdenv.isLinux (with pkgs; [inotify-tools]);

  languages.elixir = {
    enable = true;
    package = elixir;
  };
}
