defmodule GlitchtipErrorFactoryWeb.PageController do
  use GlitchtipErrorFactoryWeb, :controller

  require Logger

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.

    # NOTE: this should send an event to sentry
    Logger.info("Running a non-existent function to trigger a Sentry event")
    apply(:explosion!, [])
    render(conn, :home, layout: false)
  end
end
