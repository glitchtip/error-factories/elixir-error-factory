FROM bitwalker/alpine-elixir-phoenix:latest

arg GLITCHTIP_ERROR_FACTORY_DSN

WORKDIR /app

COPY mix.exs .
COPY mix.lock .

CMD mix deps.get && mix sentry.send_test_event && mix phx.server
